<?php 


/*

	What do i need for template.php
	
	theme_setting: basecolor
	change the upload fields
	change fieldsets to divs
	add breadcrumb
	override theme_form for xhtml compliance

*/
/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
*/
function fourseasons_preprocess_page(&$variables) {
	$base_color = theme_get_setting('fourseasons-basecolor');
	drupal_add_js('
	jQuery(document).ready(function(){
		jQuery("h1, h2, h3, h4, h5").css("color", "#'. $base_color.'");
		jQuery("#secondary").css("background", "#'. $base_color.'");
		jQuery(".pseudo-fieldset-content").hide();
		
			jQuery("#collapse-all-fieldsets").click( function () {
			  jQuery(".pseudo-fieldset-content").hide();
			  jQuery(".pseudo-fieldset").addClass("collapsed");
			});
			jQuery("#open-all-fieldsets").click( function () {
			  jQuery(".pseudo-fieldset-content").show();
			  jQuery(".pseudo-fieldset").addClass("collapsed");
			});
			
			jQuery(".collapsible .pseudo-fieldset-title").click( function () {
			  var thisFieldset = jQuery(this).parent();
			  jQuery(".pseudo-fieldset-content", thisFieldset).slideToggle();
			  jQuery(thisFieldset).toggleClass("collapsed");
			});
		  
		  });
	', 'inline');
}

/**
 * Override of theme_file().
 * Reduce the size of the upload fields.
 */
function fourseasons_file($variables) {

 $element = $variables['element'];
 $element['#attributes']['type'] = 'file';
 $element['#attributes']['size'] = '20';
 
 element_set_attributes($element, array('id', 'name', 'size'));
 _form_set_class($element, array('form-file'));

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}

/**
 * Override of theme_fieldset().
 * We use divs instead of fieldsets for better theming.
*/

function fourseasons_fieldset($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id'));
  _form_set_class($element, array('form-wrapper'));

  $output = '<div' . drupal_attributes($element['#attributes']) . '>';
  if (!empty($element['#title'])) {
    // Always wrap fieldset legends in a SPAN for CSS positioning.
    $output .= '<div class="pseudo-fieldset-title">'. $element['#title'] .'</div>';
  }
   $output .= '<div class="pseudo-fieldset-content">';
  if (!empty($element['#description'])) {
    $output .= '<div class="fieldset-description">' . $element['#description'] . '</div>';
  }
  $output .= $element['#children'];
  if (isset($element['#value'])) {
    $output .= $element['#value'];
  }
  $output .= '</div>';
  $output .= "</div>\n";
  return $output;
}

/*
 * Override of theme_form().
*/
function fourseasons_form($variables) {
  $element = $variables['element'];
$action = $element['#action'] ? 'action="'. check_url($element['#action']) .'" ' : '';
  if (isset($element['#action'])) {
    $element['#attributes']['action'] = drupal_strip_dangerous_protocols($element['#action']);
  }
  if ($element['#id'] == 'system-modules') {
    return "<a href='#' onclick='javascript:return false;' id='collapse-all-fieldsets'>".t('Collapse all fieldsets')."</a> | <a href='#' onclick='javascript:return false;' id='open-all-fieldsets'>".t('Open all fieldsets').'</a><form '. $action .' accept-charset="UTF-8" method="'. $element['#method'] .'" id="'. $element['#id'] .'"'. drupal_attributes($element['#attributes']) .">\n<div>". $element['#children'] ."\n</div></form>\n";    
  } else {
	return '<form '. $action .' accept-charset="UTF-8" method="'. $element['#method'] .'" id="'. $element['#id'] .'"'. drupal_attributes($element['#attributes']) .">\n<div>". $element['#children'] ."\n</div></form>\n";   
  }
}
//*/
/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.

function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb"><span>'. implode('</span> <span class="breadcrumb-separator">&rsaquo;</span> <span>', $breadcrumb) .'</span></div>';
  }
}
 */