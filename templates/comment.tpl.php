<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ($comment->status == COMMENT_NOT_PUBLISHED) ? ' comment-unpublished' : ''; print ' '. $zebra; ?>">
  <div class="clear-block">

  <?php if ($comment->new) : ?>
    <a id="new"></a>
    <span class="new"><?php print drupal_ucfirst($new) ?></span>
  <?php endif; ?>

  <?php print $picture; ?>

    <h3><?php print $title; ?></h3>

    <?php if ($submitted): ?>
      <span class="submitted"><?php print t('@date — !username', array('!username' => theme('username', array('comment' => $comment)), '@date' =>  format_date($comment->created, 'medium'))); ?></span>
    <?php endif; ?>
      
    <div class="content">
      <?php print render ($content); ?>
    </div>

  </div>

  <?php if (isset($links)): ?>
    <div class="links"><?php print $links; ?></div>
  <?php endif; ?>
  <div class="clearfix"></div>
</div>
