<?php

/**
 * Implements hook_form_system_theme_settings_alter().
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */

function fourseasons_form_system_theme_settings_alter(&$form, &$form_state)  {

// Color picker
	$_path = drupal_get_path('theme', 'fourseasons');
	drupal_add_js($_path . '/scripts/colorpicker/js/colorpicker.js');
	drupal_add_css($_path . '/scripts/colorpicker/css/colorpicker.css');
	$js = "
    function colorPickerAttach(which) {
      jQuery(which).ColorPicker({
        onSubmit: function(hsb, hex, rgb) {
          jQuery(which).val(hex);
		  jQuery(which).ColorPickerHide();
        },
        onBeforeShow: function () {
          jQuery(this).ColorPickerSetColor(this.value);
        }
      })
      .bind('keyup', function(){
        jQuery(this).ColorPickerSetColor(this.value);
      });
    }
  ";
  drupal_add_js($js, 'inline');
  // end color picker
  
  $defaultcolor = (theme_get_setting('fourseasons-basecolor')) ? theme_get_setting('fourseasons-basecolor') : 'FF7302';
  
  $form['configuration'] = array(
    '#type' => 'fieldset',
	'#title' => t('Configuration'),
	'#collapsible' => false,
  );
  // Create the form widgets using Forms API
  $form['configuration']['fourseasons-basecolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Four Seasons Base Color'),
	'#size' => 12,
	'#maxlength' => 6,
	'#description' => t('Choose the basecolor. Default is #FF7302'),
	'#default_value' => $defaultcolor,
	'#suffix' => "
      <script type='text/javascript'>
        jQuery(document).ready(function(){
          colorPickerAttach('#edit-fourseasons-basecolor');
        });
      </script>"
	);
	
  return $form;
}
